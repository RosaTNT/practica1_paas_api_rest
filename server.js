//Dejando el servidor arrancado y funcionando
var express = require('express');
var app = express();
//añadimos estas lineas para  que funcione el body como entrada de Parametros
var  bodyParser = require ('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

//incluimos dos  variables, una para almacenar la base de las peticiones de la API,
//y otra para la apikey
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechurcc/collections/";
var mLabApiKey = "apiKey=HcSWv7y0QDGPXNHy1dUk9wHhcz9kpv4_";

//necesitamos un cliente para poder instanciar e iniciar una petición la exterior.
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP " + port);


// preparamos una ruta de bienvenida nuestra apitechu
app.get('/apitechu/v1',
    function(req, res){
      console.log("GET /apitechu/v1");
//devolvemos una respuesta. en formato json
      res.send(
        { "msg": "Bienvenido a la Api de la TechU Molona"
        }
    );
    }
)
// vamos a crear un login de usuario.
app.post('/apitechu/v1/login',
function(req,res){
     var users = require('./usuarios.json');

     for (user of users) {

        if (user.email == req.body.email && user.password == req.body.password) {
           user.logged = true;
           writeUserDataToFile(users);
           res.send({
                     "msg" : "Login correcto",
                     "Id. usuario" : user.id
           });
           break;
         }else{user.logged = false; }
       }

       if (user.logged == false){
         res.send("Login incorrecto");
       }
   }
);
//vamos a hacer el logout
app.post('/apitechu/v1/logout',
 function(req,res){
   var users = require('./usuarios.json');
   for (user of users) {
     if (user.email == req.body.email) {

        delete user.logged;
        writeUserDataToFile(users);
        res.send({
                  "msg" : "Logout correcto",
                  "Id usuario" : user.id
                        });
        break;
      }
   }

       res.send("no se puede logout");

 }
)
//creamos una nueva ruta funcional, que obtendra una lista de usuarios
app.get('/apitechu/v1/users',
  function(req, res){
     console.log("GET /apitechu/v1/users");
     res.sendFile('usuarios.json',{root: __dirname});
   }
)

//realizamos una nueva version de la aplicación, que accederá  a mlab
app.get('/apitechu/v2/users',
  function(req, res){
     console.log("GET /apitechu/v1/users");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + mLabApiKey,
        function(err, resMlab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
          }
          res.send(response);
        }
      )
   }
)

//busqueda por id
app.get("/apitechu/v2/users/:id",
  function(req, res){
     console.log("GET /apitechu/v1/users/:id");

//creamos la variable para recuperar la id y dibujamos la Query
     var id =  req.params.id;
     var query = 'q={"id" :  ' + id + '}';
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body[0];
             } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)

// Recuperar las cuentas de un cliente

app.get("/apitechu/v2/users/:id/accounts",
  function(req, res){
     console.log("GET /apitechu/v2/users/:id/accounts");

//creamos la variable para recuperar la id y dibujamos la Query
     var id =  req.params.id;
     var query = 'q={"usuarioID" :  ' + id + '}';
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("busca clientes");

     //hacemos un get
        httpClient.get("account?" + query +'&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo cuentas"
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body;
             } else {
                    response = {
                        "msg" : "Usuario sin cuentas"
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)
// Recuperar los movimiento de la cuenta de un cliente, ponemos post, para pasar parametros via body
app.post("/apitechu/v2/users/account/movements",
  function(req, res){
     console.log("post /apitechu/v2/users/account/movements");

//creamos la variable para recuperar la cuenta y dibujamos la query
    console.log(req.body.account + " numcliente :" + req.body.numCliente);

     var query = 'q={ "numCliente": ' + req.body.numCliente +', "account": "'+ req.body.account +'"}&s={"date" : 1}';
     console.log(query);
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("busca movimientos");

 //hacemos un get
        httpClient.get("movements?" + query +'&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo movimientos del usuario"
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body;
             } else {
                    response = {
                        "msg" : "Usuario sin movimientos"
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)
//hacemos login accediendo  a MongoDB
app.post("/apitechu/v2/users/login",
  function(req, res){
     console.log("GET /apitechu/v2/users/login");

     var email =  req.body.email;
     var password = req.body.password;
     var query = 'q={"email" : "' + email + '","password":"'+password + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                        var putBody = '{"$set":{"logged":true}}';
                        query = 'q={"id" : '+body[0].id +'}';
//actualizo la condicion//
                    httpClient.put("user?" + query + '&' + mLabApiKey,
                        JSON.parse(putBody),
                         function(errPut,resMLabPUT,bodyPUT){
                           if (errPut) {
                                response = {
                                      "msg" : "Error actualizar usuario."
                              }
                              res.status(500);
                            } else {
                              response = {"msg" : "Usuario loggeado",
                                           "email" : email,
                                          "idUsuario": body[0].id }
                              }
                                 res.send(response);
                              }
                        )
               } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
     )
     console.log("Finaliza");
   }
)

//hacemos logout accediendo  a MongoDB
app.post("/apitechu/v2/users/logout",
  function(req, res){
     console.log("GET /apitechu/v2/users/logout");

     var email =  req.body.email;
     var query = 'q={"email" : "' + email + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                        response = body[0];

                        var putBody = '{"$unset":{"logged":""}}';
                        query = 'q={"id" : '+body[0].id +'}';
//actualizo la condicion
                        httpClient.put("user?" + query + '&' + mLabApiKey,
                        JSON.parse(putBody),
                         function(errPut,resMLabPUT,bodyPUT){
                           if (errPut) {
                                response = {
                                      "msg" : "Error actualizar usuario."
                              }
                              res.status(500);
                            } else {
                              response = {"msg" : "Usuario desconectado correctamente",
                                           "email" : email,
                                          "Id. usuario": body[0].id }
                              }
                                   res.send(response);
                              }
                        )
               } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
      )
   }
)


// creamos el alta de un nuevo usuario
app.post('/apitechu/v1/users',
   function(req, res){
       console.log("POST /apitechu/v1/users");
//       console.log(req); visualizamos la variable req
//añadimos la informacion al fichero.1a opcion con cabeceras
//comprobamos que llegan

      console.log("first_name es " + req.body.first_name);
      console.log("last_name es " + req.body.last_name);
      console.log("country es " + req.body.country);

//creamos un nuevo objeto
     var newUser = {
         "first_name" : req.body.first_name,
         "last_name"  : req.body.last_name,
         "country" : req.body.country
      }
// cojo el fichero json como array y le añadimos uno, en memoria
   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);

 //ENVIAMOS EL FICHERO  como respuesta
   res.send("Usuario añadido con exito");
}
)

//borrado de un registro con un parametro en la url
app.delete("/apitechu/v1/users/:ent",
   function(req,res){
        console.log("DELETE /apitechu/v1/users/:ent");

        var users = require("./usuarios.json");
        var registro = users.splice(req.params.ent - 1, 1);
        writeUserDataToFile(users);

   res.send(
     {
        "msg" : "Usuario borrado con exito" ,
        "registro": registro

      }
    );
 }
)

//hacemos alta de nuevo usuario en MongoDB
app.post("/apitechu/v2/users/nuevo",
  function(req, res){
     console.log("POST /apitechu/v2/users/nuevo");
     console.log("nuevos datos");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

//nuevo, recuperamos el máximo valor de id para sumar uno
     var query ='q=&s={"id" : -1}';
     httpClient.get("user?" + query + '&' + mLabApiKey,
      function(err, resMLab, body) {
        if (err){ response = {
                 "msg" : "Error obteniendo usuario último usuario"
              }
              res.status(500);
            } else{
                   var newid = parseInt(body[0].id) + 1;
                   console.log("devuelve el máximo" + body[0].id);
                  console.log(newid);
                  var user ={
                    "id": newid,
                    "first_name":req.body.first_name,
                    "last_name":req.body.last_name,
                    "email": req.body.email,
                    "password" : req.body.password
                  }
                  httpClient.post("user?" + '&' + mLabApiKey, user,
                       function(errPut,resMLabPUT,bodyPUT){
                 //           console.log("dentro de la funcion");
                        if (errPut) {
                                 response = {
                                     "msg" : "Error actualizar usuario."
                                     }
                                     res.status(500);
                                   } else {
                                         response = {"msg" : "Usuario dado de alta",
                                                      "user" : user}

                                         }
                                            res.send(response);
                                         }
                                   )
    }
  })
})


//hacemos alta de cuenta en MongoDB
 app.post("/apitechu/v2/users/:id/accounts",
       function(req, res){
          console.log("POST /apitechu/v2/users/:id/accounts");

     //creamos un cliente que llame a la base de datos
           httpClient = requestJson.createClient(baseMlabURL);
           console.log("Cliente creado");

           var cuenta = {
             "usuarioID":req.body.usuarioID,
            "IBAN":req.body.IBAN,
            "balance":req.body.balance
            }
     //alta de la nueva cuenta
                 httpClient.post("account?" +  '&' + mLabApiKey, cuenta,
                 function(err,resPut,bodyPUT){
                       console.log("dentro de la funcion");
                       console.log(cuenta);
                        if (err) {
                           response = {
                               "msg" : "Error alta cuenta"
                               }
                               res.status(500);
                               console.log("error");
                               //borramos usuario anteriormente dada de alta
                               httpClient.delete("user?" + '&' + mLabApiKey, req.body.usuarioID,
                                    function(errDEL,resMLabDEL,bodyDEL){
                                           console.log("dentro de la funcionde borrado");
                                     if (errDEL) {
                                              response = {
                                                  "msg" : "Error borrar Datos Usuario."
                                                  }
                                                  res.status(500);
                                                } else {
                                                      response = {"msg" : "Usuario no se ha podido dar de alta",
                                                                   "id" : req.body.usuarioID}

                                                      }
                                                         res.send(response);
                                                      }
                                                )

                             } else {
                                   response = {"msg" : "Cuenta dado de alta",
                                                "cuenta" : cuenta}
                                   res.send(response);

                                   }
                    }
                             )
})

//hacemos ingreso en cuenta
app.post("/apitechu/v2/users/accounts/ingreso",
  function(req, res){
     console.log("POST /apitechu/v2/users/accounts/ingreso");

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("vamos a hacer un ingreso");

//nuevo, recuperamos el saldo de la cuenta
      var query = 'q={"usuarioID" : '+ req.body.numCliente +', "IBAN": "'+ req.body.account +'"}';
      console.log(query);
        httpClient.get("account?" + query + '&' + mLabApiKey,
           function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              console.log("Error al hacer ingreso.")
              response = { "msg" : "Error al hacer ingreso" }
              res.status(500);
        }else{
           if (body.length > 0) {
                   response = body[0];
                   console.log("saldo" + body[0].balance);
                   body[0].balance = parseFloat(body[0].balance) + parseFloat(req.body.import);
                   console.log("balance" + body[0].balance);
                   console.log("Actualizamos el saldo de la cuenta" + body[0].balance);

                   var putBody =  '{"$set":{"balance":' +  body[0].balance +'}}';

              //     var query = 'q={"usuarioID" : '+ req.body.account +', "IBAN": "'+ req.body.account +'"}';
                   console.log(query);
                   httpClient.put("account?" + query + '&' + mLabApiKey,
                   JSON.parse(putBody),

                    function(errPut,resMLabPUT,bodyPUT){
                      console.log(putBody);
                      if (errPut) {
                           response = {
                                 "msg" : "Error actualizar el balance"
                          }
                         res.status(500);
                       } else {
                         //recuperamos la fecha del dia
                              var meses = new Array ("Ene","Feb","Mar","Abr","May","Jun","Jul","Agos","Sep","Oct","Nov","Dic");
                              var f = new Date();
                              var fechaHoy=f.getDate() + "-" + meses[(f.getMonth() +1)] + "-" + f.getFullYear();
                              console.log("fecha" + fechaHoy);

                              var movimiento ={
                                "numCliente": req.body.numCliente,
                                "account":req.body.account,
                                "type": "Ingreso efectivo",
                                "importe": req.body.import,
                                "date" : fechaHoy,
                                "saldo" : body[0].balance
                              }


                              httpClient.post("movements?" + query + '&' + mLabApiKey,
                              movimiento,
                               function(errPOST,resMLabPOST,bodyPOST){
                                 if (errPOST) {
                                      response = {
                                            "msg" : "Error actualizar movimiento"}
                                    res.status(500);
                                  } else {
                                    console.log("Ingreso realizado correctamente")
                                     response = {"msg" : "Ingreso Realizado Correctamente" }
                                  }
                               }
                            )
                              res.send(response);
                         }
                       }
                   )
        }
      }})
}
)


function writeUserDataToFile (data){

  var fs = require('fs');   //invocamos a la libreria
  var jsonUserData = JSON.stringify(data);  //convierte a texto un binario

  fs.writeFile (
        "./usuarios.json",
         jsonUserData,
         "utf-8",
         function(err){
           if (err){
              console.log(err);
           }else{
              console.log("Fichero persistido con éxito");
           }
       }
   );
}


//
app.post("/apitechu/v1/monstruo/:p1/:p2",
   function(req, res){

     console.log("Parametros");
     console.log(req.params);

     console.log("Query String");
     console.log(req.query);

     console.log("Headers");
     console.log(req.headers);

     console.log("Body");
     console.log(req.body);

   }
);
