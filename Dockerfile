
#Imagen raíz
FROM node

#Carpeta raíz
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

#Añadir volumen
VOLUME ['/logs']

#Exponer puerto
EXPOSE 3000

#Instalar dependencias
#RUN npm install (luego descomentar)

# Comando de inicialización
CMD ["npm","start"]
