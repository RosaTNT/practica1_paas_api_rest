var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

// arrancamos el servidor,para levantar la aplicación
var server = require("../server.js");
chai.use(chaihttp);

var should = chai.should();

describe('Test de API Usuarios',
 function() {
   it('Prueba que la API de usuarios responde correctamente.',
     function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
//la respuesta debe tener valor 200
             res.should.have.status(200);
//la respuesta del cuerpo del mensaje debe ser igual a Bienvenido....
             res.body.msg.should.be.eql("Bienvenido a la Api de la TechU Molona")
             done();
           }
         )
     }
   ),
   it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('email');
             user.should.have.property('password');
           }
           done();
         }
       )
     }
   )
 }
);

describe ('First test',
  function(){
    it ('Test that DuckDuckGo  works', function(done){

    chai.request('http://www.duckduckgo.com')
      .get('/')
      .end(
        function(err,res){
          console.log("Request has ended");
//          console.log(res);
          console.log(err);
// incluimos una validación de resultado de la prueba. hacemos referencia a la
// variable should definida arriba
          res.should.have.status(200)        ;
          done();
        }
      );
    });
  }
);
