//Dejando el servidor arrancado y funcionando
var express = require('express');
var app = express();
//añadimos estas lineas para  que funcione el body como entrada de Parametros
var  bodyParser = require ('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

//incluimos dos  variables, una para almacenar la base de las peticiones de la API,
//y otra para la apikey
var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechurcc/collections/";
var mLabApiKey = "apiKey=HcSWv7y0QDGPXNHy1dUk9wHhcz9kpv4_";

//necesitamos un cliente para poder instanciar e iniciar una petición la exterior.
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto BIP BIP BIP " + port);


// preparamos una ruta de bienvenida nuestra apitechu
app.get('/apitechu/v1',
    function(req, res){
      console.log("GET /apitechu/v1");
//devolvemos una respuesta. en formato json
      res.send(
        { "msg": "Bienvenido a la Api de la TechU Molona"
        }
    );
    }
)
// vamos a crear un login de usuario.
app.post('/apitechu/v1/login',
function(req,res){
     var users = require('./usuarios.json');

     for (user of users) {

        if (user.email == req.body.email && user.password == req.body.password) {
           user.logged = true;
           writeUserDataToFile(users);
           res.send({
                     "msg" : "Login correcto",
                     "Id. usuario" : user.id
           });
           break;
         }else{user.logged = false; }
       }

       if (user.logged == false){
         res.send("Login incorrecto");
       }
   }
);
//vamos a hacer el logout
app.post('/apitechu/v1/logout',
 function(req,res){
   var users = require('./usuarios.json');
   for (user of users) {
     if (user.email == req.body.email) {

        delete user.logged;
        writeUserDataToFile(users);
        res.send({
                  "msg" : "Logout correcto",
                  "Id usuario" : user.id
                        });
        break;
      }
   }

       res.send("no se puede logout");

 }
)
//creamos una nueva ruta funcional, que obtendra una lista de usuarios
app.get('/apitechu/v1/users',
  function(req, res){
     console.log("GET /apitechu/v1/users");
     res.sendFile('usuarios.json',{root: __dirname});
   }
)

//realizamos una nueva version de la aplicación, que accederá  a mlab
app.get('/apitechu/v2/users',
  function(req, res){
     console.log("GET /apitechu/v1/users");
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + mLabApiKey,
        function(err, resMlab, body){
          var response = !err ? body : {
            "msg" : "Error obteniendo usuarios."
          }
          res.send(response);
        }
      )
   }
)

//busqueda por id
app.get("/apitechu/v2/users/:id",
  function(req, res){
     console.log("GET /apitechu/v1/users/:id");

//creamos la variable para recuperar la id y dibujamos la Query
     var id =  req.params.id;
     var query = 'q={"id" :  ' + id + '}';
//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");

     //hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores

           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
            } else {
               if (body.length > 0) {
                        response = body[0];
             } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               };
              res.status(404);
       }
     }
     res.send(response);
   }
      )
   }
)
//hacemos login accediendo  a MongoDB
app.post("/apitechu/v2/users/login",
  function(req, res){
     console.log("GET /apitechu/v2/users/login");

     var email =  req.body.email;
     var password = req.body.password;
     var query = 'q={"email" : "' + email + '","password":"'+password + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                        var response = " ";

                        setLoggedTrue(body[0].id,response);

                        res.send(response);
//                        var putBody = '{"$set":{"logged":true}}';
//                        query = 'q={"id" : '+body[0].id +'}';
////actualizo la condicion//
//                        httpClient.put("user?" + query + '&' + mLabApiKey,
//                        JSON.parse(putBody),
//                         function(errPut,resMLabPUT,bodyPUT){
//                           if (errPut) {
//                                response = {
//                                      "msg" : "Error actualizar usuario."
//                              }
//                              res.status(500);
//                            } else {
//                              response = {"msg" : "Usuario loggeado",
//                                           "email" : email,
//                                          "Id. usuario": body[0].id }
//                              }
//                                   res.send(response);
//                              }
//                        )
               } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
     )
     console.log("Finaliza");
   }
)

//hacemos logout accediendo  a MongoDB
app.post("/apitechu/v2/users/logout",
  function(req, res){
     console.log("GET /apitechu/v2/users/logout");

     var email =  req.body.email;
     var query = 'q={"email" : "' + email + '"}';

//creamos un cliente que llame a la base de datos
      httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");
//hacemos un get
        httpClient.get("user?" + query + '&' + mLabApiKey,
        function(err, resMLab, body) {
//realizamos gestion de errores
           if (err) {
              response = {
                      "msg" : "Error obteniendo usuario."
              }
              res.status(500);
              } else {
               if (body.length > 0) {
                        response = body[0];

                        var putBody = '{"$unset":{"logged":""}}';
                        query = 'q={"id" : '+body[0].id +'}';
//actualizo la condicion
                        httpClient.put("user?" + query + '&' + mLabApiKey,
                        JSON.parse(putBody),
                         function(errPut,resMLabPUT,bodyPUT){
                           if (errPut) {
                                response = {
                                      "msg" : "Error actualizar usuario."
                              }
                              res.status(500);
                            } else {
                              response = {"msg" : "Usuario desconectado correctamente",
                                           "email" : email,
                                          "Id. usuario": body[0].id }
                              }
                                   res.send(response);
                              }
                        )
               } else {
                    response = {
                        "msg" : "Usuario no encontrado."
               }
               res.status(404);
               res.send(response);
               }
     }
   }
      )
   }
)


// creamos el alta de un nuevo usuario
app.post('/apitechu/v1/users',
   function(req, res){
       console.log("POST /apitechu/v1/users");
//       console.log(req); visualizamos la variable req
//añadimos la informacion al fichero.1a opcion con cabeceras
//comprobamos que llegan

      console.log("first_name es " + req.body.first_name);
      console.log("last_name es " + req.body.last_name);
      console.log("country es " + req.body.country);

//creamos un nuevo objeto
     var newUser = {
         "first_name" : req.body.first_name,
         "last_name"  : req.body.last_name,
         "country" : req.body.country
      }
// cojo el fichero json como array y le añadimos uno, en memoria
   var users = require('./usuarios.json');
   users.push(newUser);
   writeUserDataToFile(users);

 //ENVIAMOS EL FICHERO  como respuesta
   res.send("Usuario añadido con exito");
}
)

//borrado de un registro con un parametro en la url
app.delete("/apitechu/v1/users/:ent",
   function(req,res){
        console.log("DELETE /apitechu/v1/users/:ent");

        var users = require("./usuarios.json");
        var registro = users.splice(req.params.ent - 1, 1);
        writeUserDataToFile(users);

   res.send(
     {
        "msg" : "Usuario borrado con exito" ,
        "registro": registro

      }
    );
 }
)


function writeUserDataToFile (data){

  var fs = require('fs');   //invocamos a la libreria
  var jsonUserData = JSON.stringify(data);  //convierte a texto un binario

  fs.writeFile (
        "./usuarios.json",
         jsonUserData,
         "utf-8",
         function(err){
           if (err){
              console.log(err);
           }else{
              console.log("Fichero persistido con éxito");
           }
       }
   );
}

function setLoggedTrue (iden,respuesta){

  var putBody = '{"$unset":{"logged":""}}';
  query = 'q={"id" : '+ iden +'}';
//actualizo la condicion
  httpClient.put("user?" + query + '&' + mLabApiKey,
  JSON.parse(putBody),
   function(errPut,resMLabPUT,bodyPUT){
     if (errPut) {
          respuesta= {
                "msg" : "Error actualizar usuario."
        }
        res.status(500);
      } else {
        respuesta = {"msg" : "Usuario logeado correctamente",
                    "Id. usuario": iden }
        console.log(respuesta);
          }
      }
    );
}

//
app.post("/apitechu/v1/monstruo/:p1/:p2",
   function(req, res){

     console.log("Parametros");
     console.log(req.params);

     console.log("Query String");
     console.log(req.query);

     console.log("Headers");
     console.log(req.headers);

     console.log("Body");
     console.log(req.body);

   }
);
